FROM motech/java:oracle-jdk7

RUN apt-get install -y maven wget curl 

# Get the source
#COPY couchdb-lucene-1.0.2.tar.gz /opt/couchdb-lucene-1.0.2.tar.gz
#RUN tar xzf /opt/couchdb-lucene-1.0.2.tar.gz
#RUN mv couchdb-lucene-1.0.2 /opt/couchdb-lucene-1.0.2.src
ADD couchdb-lucene-1.0.2 /opt/couchdb-lucene-1.0.2.src

# build couchdb-lucene
RUN cd /opt/couchdb-lucene-1.0.2.src && mvn
RUN cd /opt && tar xvzf /opt/couchdb-lucene-1.0.2.src/target/couchdb-lucene-1.0.2-dist.tar.gz 

# cleanup
RUN apt-get autoremove -y && apt-get clean -y && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /opt/couchdb-lucene-1.0.2.src

RUN adduser --system --disabled-password --home /opt/couchdb-lucene-1.0.2 --no-create-home --shell=/bin/bash --group --gecos "" couchdb 

ADD ./opt/start_couch_lucene /opt/start_couch_lucene

ADD ./opt/couchdb-lucene.ini /opt/couchdb-lucene-1.0.2/conf/couchdb-lucene.ini

ADD ./opt/run /opt/couchdb-lucene-1.0.2/bin/

RUN chown -R couchdb:couchdb /opt/couchdb-lucene-1.0.2
RUN chmod a+x /opt/start_couch_lucene
RUN chmod a+x /opt/couchdb-lucene-1.0.2/bin/run

RUN mkdir /opt/couchdb-lucene-1.0.2/indexes
RUN chown -R couchdb:couchdb /opt/couchdb-lucene-1.0.2/indexes
RUN chmod a+w /opt/couchdb-lucene-1.0.2/indexes

VOLUME ["/opt/couchdb-lucene-1.0.2/indexes"]

# USER couchdb
CMD ["/opt/start_couch_lucene"]

EXPOSE 5985
